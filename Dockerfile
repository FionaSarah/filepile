FROM python:3.7

ENV DJANGO_SETTINGS_MODULE "core.settings.local"
ENV PYTHONUNBUFFERED 1

RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add -
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
RUN apt-get -qq update
RUN apt-get install -y postgresql-client-10 --show-progress

RUN mkdir -p -m 755 /uploads

WORKDIR /app

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt

ADD . /app
WORKDIR /app/backend
