Filepile File Upload Site
=========================

This is a simple site for doing file uploads, similar to WeTransfer.

It runs on Django and React and is containerised.

Installation
------------

 * Have Docker installed.
 * Copy `.env.example` to `.env` and make changes as required.
 * Run `docker-compose build` and `docker-compose up`
 * Go to `http://127.0.0.1:8000` and upload some files.

Running Tests
-------------

The Pytest test suite can be run on a running stack by executing `docker-compose exec backend pytest` 