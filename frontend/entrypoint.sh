#!/usr/bin/bash

echo "[REACT] Starting $NODE_ENV server."
yarn install --non-interactive

if [ ! -d /app/frontend/build ]; then
    echo "Running yarn build..."
    cd /app/frontend/src
    yarn build --production
fi

yarn start
exec "$@"