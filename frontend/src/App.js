import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import CreatePilePage from "./components/CreatePilePage";
import ViewPilePage from "./components/ViewPilePage";


function App() {
  return (
      <div className="App">
          <Jumbotron>
              <h1>Filepile</h1>
              <h2>Put a file on the pile!</h2>
          </Jumbotron>
          <Router>
              <Switch>
                  <Route path="/piles/:pileSlug">
                      <ViewPilePage />
                  </Route>
                  <Route path="/">
                      <CreatePilePage />
                  </Route>
              </Switch>
          </Router>
      </div>
  );
}

export default App;
