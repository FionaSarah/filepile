import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


function ViewPilePage()
{
    const [pileData, setPileData] = useState(null);
    const [passwordRequired, setPasswordRequired] = useState(false);
    const [passwordProvided, setPasswordProvided] = useState(false);
    const [passwordField, setPasswordField] = useState('');
    const { pileSlug } = useParams();

    useEffect(() => {
        if(pileData === null)
        {
            let url = '/api/piles/'+pileSlug+'/';
            if(passwordRequired && passwordProvided !== false)
                url += '?password='+passwordField;
            axios.get(url).then(
                response => {
                    setPileData(response.data);
                }
            ).catch(
                error => {
                    if(error.response.status === 401)
                    {
                        setPasswordRequired(true);
                        setPasswordProvided(false);
                    }
                }
            );
        }
    }, [passwordProvided]);

    if(passwordRequired && pileData === null)
    {
        let passwordFormOnSubmit = e => {
            e.preventDefault();
            setPasswordProvided(true);
        };
        return (<Form className="password-form" onSubmit={passwordFormOnSubmit}>
            <div className='help'>This Pile requires a password.</div>
            <Form.Group as={Row}>
                <Form.Label column sm={4}>
                    What is the password?
                </Form.Label>
                <Col sm={8}>
                    <Form.Control name="password" type="text" onChange={(e)=>setPasswordField(e.target.value)}/>
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Col sm={{ span: 8, offset: 4 }}>
                    <Button type="submit">Let Me In!</Button>
                </Col>
            </Form.Group>
        </Form>);
    }

    if(pileData !== null)
    {
        // Weird browser hack to serve downloaded files directly
        let serveFile = file => {
            let url = '/api/piles/'+pileData.slug+'/download/'+file.slug+'/';
            if(passwordRequired && passwordProvided !== false)
                url += '?password='+passwordField;
            axios.get(url).then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data], {type: file.content_type}));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', file.filename);
                document.body.appendChild(link);
                link.click();
                link.remove();
            })
        };

        let fileList = [];
        pileData.files.map(f => {
            fileList.push(<div><strong>{f.filename}</strong> - <Button size="sm" onClick={() => serveFile(f)}>Download</Button></div>);
        });
        //[{"filename":"asuka.mp4","slug":"GHwoxQgRvtuahEDWqscGSX"}]}

        return (<div>
            <div className='help'>The files in your Pile are available for download..</div>
            {fileList}
        </div>);
    }

    return <div>Rumaging through your Pile...</div>;
}

export default ViewPilePage;