import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


function UpdateFileList(currFileList, index, event)
{
    currFileList[index] = event.target.files[0];
    return currFileList;
}

function CreatePilePage()
{
    const [numUploadFields, setNumUploadFields] = useState(1);
    const [usePassword, setUsePassword] = useState(false);
    const [password, setPassword] = useState(null);
    const [fileList, setFileList] = useState({});
    const [doCreate, setDoCreate] = useState(false);
    const [uploadState, setUploadState] = useState(0);
    const [pileData, setPileData] = useState(null);
    const [fileUploading, setFileUploading] = useState(0);
    const [filesUploaded, setFilesUplodaded] = useState([]);
    const [done, setDone] = useState(false);

    useEffect(() => {if(uploadState > 0) setDoCreate(true)}, [uploadState]);
    useEffect(() => {
        // Initializing a pile
        if(uploadState === 1)
        {
            let data = password !== null ? {password:password} : {};
            axios.post('/api/piles/', data).then(
                res => {
                    setPileData(res.data);
                    setUploadState(2);
                }
            );
        }
        // Adding files to pile
        if(uploadState === 2)
        {
            // Prevent uploading multiple times
            let currentFile = fileList[fileUploading];
            if(filesUploaded.indexOf(currentFile.name) > -1)
                return;
            const formData = new FormData();
            formData.append('file', fileList[fileUploading]);
            axios.post('api/piles/'+pileData.slug+'/upload/', formData).then(
                res => {
                    filesUploaded.push(fileList[fileUploading].name);
                    setFilesUplodaded(filesUploaded);
                    if(fileUploading === Object.keys(fileList).length-1)
                        setUploadState(3);
                    else
                        setFileUploading(fileUploading+1);
                }
            );
        }
        // Finalizing pile
        if(uploadState === 3)
        {
            axios.post('api/piles/'+pileData.slug+'/finalize/', {}).then(
                res => {
                    setPileData(res.data);
                    setDone(true);
                }
            );
        }
    }, [uploadState, fileUploading]);

    // Finished creating pile, give the user a url
    if(done)
    {
        const fullUrl = "http://"+window.location.host+"/piles/"+pileData.slug+"/";
        return <div className="pile-done">
            <div>Your Filepile is ready to share at the following URL:</div>
            <div><a href={fullUrl}>{fullUrl}</a></div>
        </div>;
    }

    // Displaying status update of upload process
    if(doCreate)
    {
        let statusUpdate = [<div>Creating Your Pile...</div>];
        if(uploadState > 1)
        {
            filesUploaded.map(f => statusUpdate.push(<div>Added <strong>{f}</strong> to the Pile!</div>));
            if(filesUploaded.length < Object.keys(fileList).length)
                statusUpdate.push(<div>Putting <strong>{fileList[fileUploading].name}</strong> on the Pile...</div>);
        }
        if(uploadState > 2)
            statusUpdate.push(<div>Finalizing Your Pile...</div>);
        return <div className="upload-status">{statusUpdate}</div>
    }

    // Normal form
    return (
        <Form className="file-form" method="POST">
            <div className='help'>Put a file on a Pile and send the Pile to a friend.</div>
            {[...Array(numUploadFields)].map((x, i) =>
                <Form.Group as={Row} key={i}>
                    <Form.Label column sm={4}>
                        Upload a file
                    </Form.Label>
                    <Col sm={8}>
                        <Form.Control key={i} name={"file_"+i}  type="file" onChange={(e)=>setFileList(UpdateFileList(fileList, i, e))}/>
                    </Col>
                </Form.Group>
            )}
            <Form.Group as={Row}>
                <Col sm={4}>
                    <Button size="sm" variant="outline-info" onClick={() => setNumUploadFields(numUploadFields+1)}>+ Another File</Button>
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Form.Label column sm={4}>
                    Protect your Pile with a password?
                </Form.Label>
                <Col sm={8}>
                    {
                        usePassword ? <Form.Control name="password" type="text" onChange={(e)=>setPassword(e.target.value)}/>
                        : <Form.Check label="Yup" onClick={() => setUsePassword(true)}/>
                    }
                </Col>
            </Form.Group>
            <Form.Group as={Row}>
                <Col sm={{ span: 8, offset: 4 }}>
                    <Button type="submit" onClick={()=>setUploadState(1)}>Create Pile!</Button>
                </Col>
            </Form.Group>
        </Form>
    );
}

export default CreatePilePage;