import os

from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings


class ReactAppView(View):
    """
    Serves the compiled frontend
    """
    def get(self, request):
        if settings.NODE_ENV == "production":
            with open(os.path.join(settings.FRONTEND_DIR, 'build', 'index.html')) as f:
                return HttpResponse(f.read())
        else:
            return HttpResponseRedirect(settings.FRONTEND_URL)
