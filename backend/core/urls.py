from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from react.views import ReactAppView

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api-auth/', include('rest_framework.urls')),

    path('api/', include('api.urls')),

    url(r'^', ReactAppView.as_view()),
]
