from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils.functional import cached_property


class UploadedFdlesStorage(FileSystemStorage):
    """
    Storage backend for uploaded files to put them outside of MEDIA_ROOT.
    """
    @cached_property
    def base_location(self):
        return settings.FILE_UPLOAD_PATH

    def url(self, name):
        raise ValueError("Not accessible via URL.")
