from .base import *

DEBUG = True

NODE_ENV = os.getenv('NODE_ENV', default="development")