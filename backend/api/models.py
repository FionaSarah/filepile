import os
from datetime import datetime, timezone

from django.db import models
from django.conf import settings
import shortuuid


def slug_uuid():
    return shortuuid.uuid()


class Pile(models.Model):
    """
    Files are stored in Piles.
    """
    date_created = models.DateTimeField(auto_now_add=True)
    password = models.CharField(max_length=255, null=True, blank=True)
    date_expires = models.DateTimeField(null=True, blank=True)
    slug = models.CharField(max_length=255, unique=True, default=slug_uuid, null=True)
    finalized = models.BooleanField(default=False)

    @property
    def has_expired(self):
        """Boolean representing if the Pile has expired and cannot be downloaded."""
        if self.date_expires is None:
            return False
        return self.date_expires < datetime.now(timezone.utc)

    def __str__(self):
        num_files = self.files.count()
        return f'Pile ({self.slug}), {num_files} number of files, created {self.date_created}'


class File(models.Model):
    """
    Individual files for download
    """
    pile = models.ForeignKey(Pile, on_delete=models.CASCADE, null=False, related_name='files')
    filename = models.CharField(max_length=255, help_text='The original filename the file was uploaded with.')
    slug = models.CharField(max_length=255, unique=True, default=slug_uuid)
    size = models.IntegerField(null=True)
    content_type = models.CharField(max_length=255, null=True)

    @property
    def filesystem_location(self):
        """The directory that the file is stored in."""
        return os.path.join(settings.FILE_UPLOAD_PATH, self.pile.slug)

    @property
    def full_filesystem_path(self):
        """A full path including file to the location of the file."""
        return os.path.join(self.filesystem_location, self.slug)

    def store_to_filesystem(self, file_object):
        """
        Given an UploadedFile object, this will write it to a more permanent location and update the
        relevant fields.
        """
        if self.filename:
            raise OSError('File object already has a file stored.')
        # Plop on filesystem
        if not os.path.isdir(self.filesystem_location):
            os.makedirs(self.filesystem_location)
        with open(self.full_filesystem_path, 'wb+') as stored_file:
            for chunk in file_object.chunks():
                stored_file.write(chunk)
        # Remember original filename and size
        self.filename = file_object.name
        self.content_type = file_object.content_type
        self.size = file_object.size
        self.save()
