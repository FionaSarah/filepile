import os
from datetime import datetime, timezone, timedelta

import pytest
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from api.models import File, Pile


@pytest.mark.django_db
class TestPileModel:
    @pytest.fixture
    def pile(self):
        return Pile.objects.create()

    def test_has_expired_false_when_no_expiry_date(self, pile):
        assert not pile.has_expired

    def test_has_expired_false_when_not_expired(self, pile):
        pile.date_expires = datetime.now(timezone.utc) + timedelta(days=5)
        pile.save()
        assert not pile.has_expired

    def test_has_expired_true_when_expired(self, pile):
        pile.date_expires = datetime.now(timezone.utc) - timedelta(days=5)
        pile.save()
        assert pile.has_expired


@pytest.mark.django_db
class TestFileModel:
    @pytest.fixture
    def pile(self):
        return Pile.objects.create()

    @pytest.fixture
    def image_file(self, pile):
        return File.objects.create(pile=pile)

    @pytest.fixture
    def uploaded_file(self):
        return SimpleUploadedFile('kitten.jpg', b'imaginesomejpeghere', content_type='image/jpg')

    def test_store_to_filesystem_updates_fields(self, pile, image_file, uploaded_file):
        image_file.store_to_filesystem(uploaded_file)
        assert image_file.size == uploaded_file.size
        assert image_file.filename == uploaded_file.name
        assert image_file.content_type == uploaded_file.content_type

    def test_store_to_filesystem_saves_to_disk(self, pile, image_file, uploaded_file):
        image_file.store_to_filesystem(uploaded_file)
        path = os.path.join(settings.FILE_UPLOAD_PATH, pile.slug, image_file.slug)
        assert os.path.exists(path)
        with uploaded_file.open('rb') as uploaded:
            with open(path, 'rb') as stored:
                assert stored.read() == uploaded.read()
