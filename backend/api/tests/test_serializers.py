import pytest
from api.serializers import PileSerializer


@pytest.mark.django_db
class TestPileSerializer:

    def test_create_pile(self):
        serializer = PileSerializer(data={'password': 'test'})
        assert serializer.is_valid()
        pile = serializer.save()
        assert pile.password is 'test'
        assert pile.slug
