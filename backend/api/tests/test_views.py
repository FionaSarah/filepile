from datetime import timedelta, datetime, timezone

import pytest
import os
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from api.models import Pile, File


@pytest.mark.django_db
class TestPileAPIView:
    @pytest.fixture
    def url(self):
        return '/api/piles/'

    def test_create(self, api_client, url):
        response = api_client.post(url, {})
        assert response.data['slug']

    def test_create_password(self, api_client, url):
        response = api_client.post(url, {'password': 'test'})
        assert response.data['password'] == 'test'

    def test_ignores_provided_slug(self, api_client, url):
        response = api_client.post(url, {'slug': 'testing'})
        assert not response.data['slug'] == 'testing'


@pytest.mark.django_db
class TestGetPileAPIView:
    @pytest.fixture
    def url(self):
        return lambda pile: f'/api/piles/{pile.slug}/'

    @pytest.fixture
    def pile(self):
        pile = Pile.objects.create(finalized=True)
        File.objects.create(pile=pile, filename='cool_file.jpg')
        return pile

    def test_returns_pile_data(self, api_client, url, pile):
        response = api_client.get(url(pile))
        assert response.status_code == 200
        assert response.data['slug'] == pile.slug
        assert len(response.data['files']) == 1
        assert response.data['files'][0]['filename'] == 'cool_file.jpg'

    def test_returns_pile_data_with_password(self, api_client, url, pile):
        pile.password = 'hunter2'
        pile.save()
        response = api_client.get(url(pile) + '?password=hunter2')
        assert response.status_code == 200
        assert response.data['slug'] == pile.slug

    def test_error_if_not_exist(self, api_client, url, pile):
        url = url(pile)
        pile.delete()
        response = api_client.get(url)
        assert response.status_code == 404

    def test_error_if_not_finalized(self, api_client, url, pile):
        pile.finalized = False
        pile.save()
        response = api_client.get(url(pile))
        assert response.status_code == 404

    def test_unauthorized_if_password_required(self, api_client, url, pile):
        pile.password = 'hunter2'
        pile.save()
        response = api_client.get(url(pile))
        assert response.status_code == 401

    def test_unauthorized_if_incorrect_password(self, api_client, url, pile):
        pile.password = 'hunter2'
        pile.save()
        response = api_client.get(url(pile)+'?password=robeandwizardhat')
        assert response.status_code == 401


@pytest.mark.django_db
class TestFinalizePileAPIView:
    @pytest.fixture
    def url(self):
        return lambda pile: f'/api/piles/{pile.slug}/finalize/'

    @pytest.fixture
    def pile(self):
        pile = Pile.objects.create()
        File.objects.create(pile=pile, filename='cool_file.jpg')
        return pile

    def test_can_finalize(self, api_client, pile, url):
        response = api_client.post(url(pile))
        assert response.status_code == 200

    def test_error_if_not_exist(self, api_client, url, pile):
        url = url(pile)
        pile.delete()
        response = api_client.post(url)
        assert response.status_code == 404

    def test_error_if_already_finalized(self, api_client, url, pile):
        pile.finalized = True
        pile.save()
        response = api_client.post(url(pile))
        assert response.status_code == 400

    def test_error_if_no_files_in_pile(self, api_client, url, pile):
        pile.files.all().delete()
        response = api_client.post(url(pile))
        assert response.status_code == 400


@pytest.mark.django_db
class TestUploadFileAPIView:
    @pytest.fixture
    def url(self):
        return lambda pile: f'/api/piles/{pile.slug}/upload/'

    @pytest.fixture
    def non_exist_url(self):
        return '/api/piles/notexisting/upload/'

    @pytest.fixture
    def pile(self):
        return Pile.objects.create()

    @pytest.fixture
    def image_file(self):
        return SimpleUploadedFile('kitten.jpg', b'imaginesomejpeghere', content_type='image/jpg')

    def test_response_correct_on_upload(self, api_client, pile, url, image_file):
        response = api_client.post(
            url(pile), {'file': image_file},
            HTTP_CONTENT_DISPOSITION=f'attachment; filename={image_file.name}'
        )
        assert response.status_code == 200
        assert response.data['filename'] == image_file.name
        assert response.data['slug']

    def test_uploaded_file_was_stored(self, api_client, pile, url, image_file):
        response = api_client.post(
            url(pile),  {'file': image_file},
            HTTP_CONTENT_DISPOSITION=f'attachment; filename={image_file.name}'
        )
        path = os.path.join(settings.FILE_UPLOAD_PATH, pile.slug, response.data['slug'])
        assert os.path.exists(path)
        with image_file.open('rb') as uploaded:
            with open(path, 'rb') as stored:
                assert stored.read() == uploaded.read()

    def test_error_when_pile_doesnt_exist(self, api_client, non_exist_url, image_file):
        response = api_client.post(
            non_exist_url, {'file': image_file},
            HTTP_CONTENT_DISPOSITION=f'attachment; filename={image_file.name}'
        )
        assert response.status_code == 404

    def test_error_when_pile_finalised(self, api_client, pile, url, image_file):
        pile.finalized = True
        pile.save()
        response = api_client.post(
            url(pile), {'file': image_file},
            HTTP_CONTENT_DISPOSITION=f'attachment; filename={image_file.name}'
        )
        assert response.status_code == 404

    def test_error_when_no_file_sent(self, api_client, pile, url):
        response = api_client.post(
            url(pile),
            HTTP_CONTENT_DISPOSITION=f'attachment; filename=cool.jpg'
        )
        assert response.status_code == 400


@pytest.mark.django_db
class TestFileDownloadAPIView:
    @pytest.fixture
    def url(self):
        return lambda pile, file: f'/api/piles/{pile.slug}/download/{file.slug}/'

    @pytest.fixture
    def url_pile_not_exist(self):
        return lambda file: f'/api/piles/a_fake_pile/download/{file.slug}/'

    @pytest.fixture
    def url_file_not_exist(self):
        return lambda pile: f'/api/piles/{pile.slug}/download/a_fake_file/'

    @pytest.fixture
    def pile(self):
        return Pile.objects.create(finalized=True)

    @pytest.fixture
    def file_content(self):
        return b'imaginesomejpeghere'

    @pytest.fixture
    def image_file(self, file_content, pile):
        file = File.objects.create(
            pile=pile, filename='kitten.jpg', slug='slimeyslug', content_type='image/jpg', size=len(file_content)
        )
        os.makedirs(file.filesystem_location)
        with open(file.full_filesystem_path, 'wb+') as fh:
            fh.write(file_content)
        return file

    def test_file_content_is_provided(self, api_client, url, pile, image_file, file_content):
        response = api_client.get(url(pile, image_file))
        assert response.status_code == 200
        assert response.content == file_content

    def test_password_protected_file_is_provided(self, api_client, url, pile, image_file, file_content):
        pile.password = 'hunter2'
        pile.save()
        response = api_client.get(url(pile, image_file) + '?password=hunter2')
        assert response.status_code == 200
        assert response.content == file_content

    def test_errors_when_no_password_provided(self, api_client, url, pile, image_file):
        pile.password = 'hunter2'
        pile.save()
        response = api_client.get(url(pile, image_file))
        assert response.status_code == 400

    def test_errors_when_pile_not_exist(self, api_client, url_pile_not_exist, pile, image_file):
        response = api_client.get(url_pile_not_exist(image_file))
        assert response.status_code == 404

    def test_errors_when_file_not_exist(self, api_client, url_file_not_exist, pile, image_file):
        response = api_client.get(url_file_not_exist(pile))
        assert response.status_code == 404

    def test_errors_when_pile_expired(self, api_client, url, pile, image_file):
        pile.date_expires = datetime.now(timezone.utc) - timedelta(days=5)
        pile.save()
        response = api_client.get(url(pile, image_file))
        assert response.status_code == 400
