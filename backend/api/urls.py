from django.urls import path

from . import views

urlpatterns = [
    path('piles/', views.PileAPIView.as_view()),
    path('piles/<str:slug>/', views.GetPileAPIView.as_view()),
    path('piles/<str:slug>/finalize/', views.FinalizePileAPIView.as_view()),

    path('piles/<str:slug>/upload/', views.UploadFileAPIView.as_view()),
    path('piles/<str:pile_slug>/download/<str:file_slug>/', views.FileDownloadAPIView.as_view()),
]
