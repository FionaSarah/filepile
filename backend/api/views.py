from django.core.files import File as DjangoFile
from django.http import HttpResponse
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.views import APIView
from rest_framework.response import Response

from api.models import Pile, File
from api.serializers import PileSerializer, FileSerializer


class PileAPIView(APIView):
    def post(self, request, format=None):
        """
        Create a new Pile for adding files to.
        """
        serializer = PileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetPileAPIView(APIView):
    """
    Get information about a Pile via slug.
    """
    def get(self, request, slug):
        try:
            pile = Pile.objects.get(slug=slug, finalized=True)
        except Pile.DoesNotExist:
            return Response('Pile with this slug not found', status=status.HTTP_404_NOT_FOUND)
        password = request.GET.get('password', None)
        if pile.password and not pile.password == password:
            return Response('A password is required to access this Pile.', status=status.HTTP_401_UNAUTHORIZED)
        return Response(PileSerializer(pile).data)


class FinalizePileAPIView(APIView):
    """
    Finalizes a pile, stopping any new files being added to it and allowing the added ones to be accessed.
    """
    def post(self, request, slug):
        try:
            pile = Pile.objects.get(slug=slug)
        except Pile.DoesNotExist:
            return Response('Pile with this slug not found', status=status.HTTP_404_NOT_FOUND)
        if pile.finalized:
            return Response('Pile already finalized.', status=status.HTTP_400_BAD_REQUEST)
        if not pile.files.count():
            return Response('A Pile without files cannot be finalized.', status=status.HTTP_400_BAD_REQUEST)
        pile.finalized = True
        pile.save()
        return Response(PileSerializer(pile).data)


class UploadFileAPIView(APIView):
    """
    Uploads a single file to a Pile.
    """
    parser_classes = [MultiPartParser]

    def post(self, request, slug):
        try:
            pile = Pile.objects.get(slug=slug, finalized=False)
        except Pile.DoesNotExist:
            return Response('Open Pile with this slug not found', status=status.HTTP_404_NOT_FOUND)
        if 'file' not in request.data:
            return Response('File not provided', status=status.HTTP_400_BAD_REQUEST)
        new_file = File(pile=pile)
        new_file.save()
        new_file.store_to_filesystem(request.data['file'])
        return Response(FileSerializer(new_file).data)


class FileDownloadAPIView(APIView):
    """
    Download a single file
    """
    def get(self, request, pile_slug, file_slug):
        # Get objects
        try:
            pile = Pile.objects.get(slug=pile_slug, finalized=True)
        except Pile.DoesNotExist:
            return Response('Pile with this slug not found', status=status.HTTP_404_NOT_FOUND)
        try:
            file = File.objects.get(slug=file_slug, pile=pile)
        except File.DoesNotExist:
            return Response('File with this slug not found', status=status.HTTP_404_NOT_FOUND)
        # Simple authentication
        password = request.GET.get('password', None)
        if pile.password and not pile.password == password:
            return Response('A password is required to access this file.', status=status.HTTP_400_BAD_REQUEST)
        # Check expiry
        if pile.has_expired:
            return Response('This file has expired and cannot be downloaded.', status=status.HTTP_400_BAD_REQUEST)
        # Prepare and return response
        fh = open(file.full_filesystem_path, 'rb')
        response = HttpResponse(DjangoFile(fh), content_type=file.content_type)
        response['Content-Disposition'] = f'attachment; filename="{file.filename}"'
        return response
