from rest_framework import serializers

from api.models import Pile


class FileSerializer(serializers.Serializer):
    filename = serializers.CharField(read_only=True)
    slug = serializers.CharField(read_only=True)
    content_type = serializers.CharField(read_only=True)


class PileSerializer(serializers.Serializer):
    password = serializers.CharField(required=False, allow_blank=True)
    date_expires = serializers.DateTimeField(required=False)
    slug = serializers.CharField(required=False, read_only=True)
    finalized = serializers.BooleanField(read_only=True)
    files = FileSerializer(many=True, read_only=True)

    def create(self, validated_data):
        return Pile.objects.create(**validated_data)
