import os
import shutil
import pytest

from rest_framework.test import APIClient


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture(autouse=True)
def file_upload_path():
    from django.conf import settings
    if os.path.exists(settings.FILE_UPLOAD_PATH):
        shutil.rmtree(settings.FILE_UPLOAD_PATH)
    os.makedirs(settings.FILE_UPLOAD_PATH)
    yield
    shutil.rmtree(settings.FILE_UPLOAD_PATH)
