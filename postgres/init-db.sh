#!/bin/bash

set -e
echo "Hello"

psql -v ON_ERROR_STOP=1 --username "$DB_USER" <<-EOSQL
    CREATE DATABASE filepile;
    GRANT ALL PRIVILEGES ON DATABASE filepile TO postgres;
EOSQL